#Usage
### 1.create folder
```
mkdir code logs db_data
```

### 2. copy env file
```
cp .env.example .env
```
> you can setting default value in env file

```shell
DB_DATA=./db_data
DB_CONFIG=./conf/mysql
CODE_DIR=./code
MYSQL_ROOT_PASSWORD=qaz112233
NGINX_CONFIG=./conf/nginx
NGINX_LOGS=./logs/nginx
```

### 3. copy your code into code folder

### 4.set nginx conf

> default conf you can see the `/conf/nginx/woo.conf`

### 5.run docker-compose

```
docker-compose up -d
```
---
### reload nginx config
> if you change nginx config,you need restart the nginx service
there are two way to restart nginx service

**enter docker container restart nginx**
```shell
# get nginx container id
docker ps
# enter nginx container
docker exec -it nginx_container_id bash
# restart nginx
service nginx restart
```

### use adminer pannel
> access mariadb

Access `127.0.0.1:8080` in your browser
defalut server: `db`
default mysql user: `root`
default mysql password: `qaz112233`

you can change this default setting in `.env`


