# Backup
docker exec container_id mysqldump -uroot --password='password' db_name > backup.sql

# Backup all database
docker exec some-mysql sh -c 'exec mysqldump --all-databases -uroot -p"$MYSQL_ROOT_PASSWORD"' > /some/path/on/your/host/all-databases.sql

#backup use docker root password
docker exec some-mysql sh -c 'exec mysqldump -uroot -p"$MYSQL_ROOT_PASSWORD" db_name > /some/path/on/your/host/all-databases.sql'

# Restore
cat backup.sql | docker exec -i CONTAINER /usr/bin/mysql -u root --password=root DATABASE