FROM daocloud.io/php:7.3-fpm

RUN docker-php-ext-install \
    pdo \
    pdo_mysql \
    mbstring \
    mysqli && docker-php-ext-enable mysqli 
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd
WORKDIR /var/www/html

